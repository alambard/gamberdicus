# Gamberdicus

Du téléchargement des données jusqu'aux outils de génomique comparative de 8 transcriptome de _Gamberdiscus sp_

## Dependencies
1. **System** : Linux or MacOS

2. **Softwares**
  - Sratool v2.10.1
  - Fastxtoolkit v0.0.14
  - Blast v2.9.0
  - Orthofinder v2.4.5
  - seqkit v2.3.1

3. **Shell programs**
  * file treatment: grep, awk, zcat
