## Download data

On utilise la commande fastq dump de sratool déja installé sur datarmor pour télécharger les transcritpome sur NCBI / sra database. Script Download.pbs.

## Decompresser les téléchargements

On a les transcritpome téléchargé en fichier compresser en format .gz. Pour les decompresser , on utilise la commande zcat de linux.
Script gunzip.pbs

## Obtenir les fasta

Les fichiers décompressés sont au format .fastq. Avec la commande fastq_to_fasta de l'outil fastxtoolkit présent sur datarmor , on peut obtenir les format fasta pour chaque fastq. 
Script fastxtool.pbs
